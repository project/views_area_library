<?php

declare(strict_types=1);

namespace Drupal\Tests\views_area_library\Kernel;

use Drupal\Tests\views\Kernel\ViewsKernelTestBase;
use Drupal\views\Views;

/**
 * Tests the library area handler.
 *
 * @group views
 */
class AreaLibraryTest extends ViewsKernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'system',
    'views_area_library',
  ];

  /**
   * {@inheritdoc}
   */
  public static $testViews = ['test_view'];

  /**
   * Tests the library area handler.
   */
  public function testAreaLibrary(): void {
    $library = 'system/base';
    $view = Views::getView('test_view');
    $view->setDisplay();
    $view->displayHandlers->get('default')->overrideOption('header', [
      'library' => [
        'id' => 'library',
        'table' => 'views',
        'field' => 'library',
        'plugin_id' => 'library',
        'library' => $library,
      ],
    ]);

    // Execute the view.
    $this->executeView($view);

    $expected_build = ['#attached' => ['library' => [$library]]];
    $build = $view->display_handler->handlers['header']['library']->render();
    $this->assertEquals($build, $expected_build);
  }

}
