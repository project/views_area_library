<?php

/**
 * @file
 * Contains views hooks.
 */

declare(strict_types=1);

use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Implements hook_views_data().
 */
function views_area_library_views_data(): array {
  $data['views']['library'] = [
    'title' => new TranslatableMarkup('Attach library'),
    'help' => new TranslatableMarkup('Attach a library to the view.'),
    'area' => [
      'id' => 'library',
    ],
  ];
  return $data;
}
