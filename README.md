# Views area library

This module provides a plugin for views,
which allows libraries to be attached to views.

## Requirements

This module requires no modules outside of Drupal core.

## Installation

Install as you would normally install a contributed Drupal module.
Visit https://www.drupal.org/node/1897420 for further information.

## Configuration

1. After installation, go to the settings of the desired view.
2. Start adding a footer or header (it doesn't matter).
3. Find and select "Attach library" from the list of options.
4. Enter the name of the library and submit the form.
