<?php

declare(strict_types=1);

namespace Drupal\views_area_library\Plugin\views\area;

use Drupal\Core\Asset\LibraryDiscoveryInterface;
use Drupal\Core\Extension\ThemeHandlerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Attribute\ViewsArea;
use Drupal\views\Plugin\views\area\AreaPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Views area library handler.
 *
 * @ingroup views_area_handlers
 */
#[ViewsArea('library')]
class Library extends AreaPluginBase {

  /**
   * The library discovery.
   */
  protected LibraryDiscoveryInterface $libraryDiscovery;

  /**
   * The theme handler.
   */
  protected ThemeHandlerInterface $themeHandler;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): static {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->libraryDiscovery = $container->get('library.discovery');
    $instance->themeHandler = $container->get('theme_handler');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions(): array {
    $options = parent::defineOptions();
    $options['library'] = ['default' => ''];
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function adminSummary() {
    $full = $this->options['library'];
    $parts = explode('/', $full, 2);
    if (!$this->libraryDiscovery->getLibraryByName($parts[0], $parts[1])) {
      $full .= ' [' . $this->t('Broken') . ']';
    }
    return $full;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state): void {
    $form['library'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Library'),
      '#default_value' => $this->options['library'],
      '#required' => TRUE,
      '#description' => $this->t('Library must be in the format "extension/library".'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function validateOptionsForm(&$form, FormStateInterface $form_state): void {
    $value = $form_state->getValue(['options', 'library']);
    $library = explode('/', $value, 2);
    if (count($library) !== 2) {
      $form_state->setErrorByName('library', $this->t('Library must be in the format "extension/library".'));
      return;
    }

    if (!$this->libraryDiscovery->getLibraryByName($library[0], $library[1])) {
      $form_state->setErrorByName('library', $this->t('Library "@library" does not exist.', ['@library' => $value]));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function render($empty = FALSE): array {
    return ['#attached' => ['library' => [$this->options['library']]]];
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies(): array {
    $dependencies = parent::calculateDependencies();
    $dependency = explode('/', $this->options['library'])[0];
    if ($this->getModuleHandler()->moduleExists($dependency)) {
      $dependencies['module'][] = $dependency;
    }
    elseif ($this->themeHandler->themeExists($dependency)) {
      $dependencies['theme'][] = $dependency;
    }
    return $dependencies;
  }

}
